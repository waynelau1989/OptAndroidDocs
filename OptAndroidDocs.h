#ifndef OPTANDROIDDOCS_H
#define OPTANDROIDDOCS_H

#include <QString>
#include <QStringList>
#include <QFileInfo>
#include <QFile>
#include <QMap>
#include <QTextCodec>

class OptAndroidDocs
{
private:
    const char*					mDocsPath;
    QMap<QString, QString> 	mTagMap;
    QStringList 				mKeyWords;
    QString						mStartTag;
    QTextCodec*					mTextCodec;
public:
    OptAndroidDocs();
    OptAndroidDocs(const char* docsPath);
    void start();

private:
    void handleDir(QDir dir);
    void handleFile(QString filePath);

    bool hasStartTag(QString line);
    bool hasEndTag(QString line);
    bool hasKeyWord(QString line);
    bool hasFinishTag(QString line);
};

#endif // OPTANDROIDDOCS_H
