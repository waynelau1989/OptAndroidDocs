#-------------------------------------------------
#
# Project created by QtCreator 2014-11-26T14:13:20
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = OptAndroidDocs
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    OptAndroidDocs.cpp

HEADERS += \
    OptAndroidDocs.h
