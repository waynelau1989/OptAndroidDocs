//#include <QCoreApplication>
#include "OptAndroidDocs.h"
#include <QDir>

int main(int argc, char *argv[])
{
    //qDebug("Current Dir:%s", QDir::currentPath().toStdString().c_str());

    /*
    for (int i=0; i<argc; i++) {
        qDebug("arg%02d:%s", argc, argv[i]);
    }
    */

    OptAndroidDocs *optAndroidDocs = NULL;

    if (argc > 1) {
        optAndroidDocs = new OptAndroidDocs(argv[1]);
    } else {
        optAndroidDocs = new OptAndroidDocs(QDir::currentPath().toStdString().c_str());
    }

    optAndroidDocs->start();

    delete optAndroidDocs;
}
