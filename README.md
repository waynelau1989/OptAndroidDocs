# OptAndroidDocs

### 背景
> 自从Google被墙了之后，访问Android的API文档一直是个问题。虽然Android SDK可以将开发文档下载下来离线查看，但是由于文档中仍然链接Google的在线资源导（js,fonts等）致访问速度仍然很慢。

* 网上大概有２种方法：
    1. 采用脚本删掉html文件中对在线资源的引用，网上也有别人处理过的可以下载(你也可以[下载](http://git.oschina.net/velable/Android_Offline_Docs)我优化过的离线文档)
    2. 是脱机查看，有人还在chrome下写了个插件。

* 但是我对以上两种方法还不够满意:
    1. Android文档更新之后，需要重新处理。脚本很慢，等待网友提供处理过的文档又不够及时。
    2. 我用常用的浏览器是Firefox

所以决定用C++重新写一个优化Android离线文档访问速度的小程序。本项目采用Qt Creator开发（Linux下这个用起来很爽，很方便）。本项目在Linux编译测试通过，理论上windows也是可以的。

本项目编译之后会生成OptAndroidDocs，在Android的文档目录下执行OptAndroidDocs即可

```
Vale@PC:~/.my_files/usr/android_develop/sdk/docs$ time OptAndroidDocs

real    0m14.163s
user    0m11.227s
sys     0m1.180s
```
从上面的数据可以看到，处理几百兆的Android 5.0离线文档(大概是9000多个html文件)
只用了14秒。


* 最后有两个文件需要手动编辑下：

1.assets/js/docs.js中：
```
  var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
  po.src = 'https://apis.google.com/js/plusone.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
```
改为:
```
  var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
  //po.src = 'https://apis.google.com/js/plusone.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
```
就是注释掉中间那一行

2.还是assets/js/docs.js中：
```
         // Search for matching JD docs
         if (text.length >= 3) {
           // Regex to match only the beginning of a word
           var textRegex = new RegExp("\\b" + text.toLowerCase(), "g");
```
改为：
```
         // Search for matching JD docs
         if (text.length >= 1000) {
           // Regex to match only the beginning of a word
           var textRegex = new RegExp("\\b" + text.toLowerCase(), "g");
```
这第二点比较重要，如果不修改在输入类名时，
文档的搜索功能不能自动补全(只能补全前两个字母)

